# satisfying 03_simon_says_spec.rb

def echo(echoed)
  echoed
end

def shout(shouted)
  shouted.upcase
end

def repeat(str, n = 2)
  accumulate = []
  n.times do
    accumulate << str
  end
  accumulate.join(' ')
end

def start_of_word(str, n)
  str[0...n]
end

def first_word(str)
  word_end = str.index(' ')
  word_end ? str[0...word_end] : str
end

def titleize(str)
  return_arr = str.split(' ').map { |s| s.length > 4 ? s.capitalize : s }
  return_arr[0].capitalize!
  return_arr[-1].capitalize!
  return_arr.join(' ')
end

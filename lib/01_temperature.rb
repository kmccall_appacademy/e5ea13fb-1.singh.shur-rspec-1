# satisfying 01_temperature_spec.rb


def ftoc(fahrenheit)
  ((fahrenheit - 32) * 5.0 / 9.0).round(1)
end

def ctof(celsius)
  (celsius * 9.0 / 5.0 + 32).round(1)
end

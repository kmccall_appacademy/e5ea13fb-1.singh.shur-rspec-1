# satisfying 02_calculator_spec.rb

def add(n1, n2)
  n1 + n2
end

def subtract(n1, n2)
  n1 - n2
end

def sum(arr)
  arr.inject(0) { |acc, i| add(acc, i) }
end

def multiply(*arr)
  arr.inject(1) { |acc, i| acc * i }
end

def power(n1, n2)
  accumulate = 1
  n2.times do
    accumulate = multiply(accumulate, n1)
  end
  accumulate
end

def factorial(n)
  return 1 if n < 2
  multiply(n, factorial(n - 1))
end

# satisfying 04_pig_latin_spec.rb

def translate(str)
  # start of str: any consonant n times then single vowel; get 0 or nil
  consonant = ->(n) { /^[a-z&&[^aeiou]]{#{n}}[aeiou]/ }
  phonemes = %w[sch qu]
  latin_end = 'ay'
  words = str.split(' ').map do |strg|
    s = strg.downcase
    ret_str = ''

    next s[3..-1] + s[0..2] + latin_end if s[1..2] == 'qu'

    phonemes.each do |e|
      break ret_str = s[e.length..-1] + e if s.start_with?(e)
    end

    # general cases
    if ret_str.empty?
      case s
      when consonant.call(0) # first char vowel
        ret_str << s
      when consonant.call(1) # second char vowel
        ret_str << s[1..-1] << s[0]
      when consonant.call(2) # third char vowel
        ret_str << s[2..-1] << s[0..1]
      when consonant.call(3) # fourth char vowel
        ret_str << s[3..-1] << s[0..2]
      end
    end
    ret_str << latin_end
    punct = ret_str.scan(/\W/)
    final = ret_str.split(/\W/).concat(punct).join('')
    if strg =~ /^[[:upper:]]/
      final.capitalize!
    else
      final
    end
  end
  words.join(' ')
end
